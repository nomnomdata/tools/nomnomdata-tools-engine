# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.15.8] - 2020-12-30

### Changed

- Model UUIDs are now included in the pre update confirmation dialog.
- Improved wording of profile not found error.
