FROM python:3.7 as builder

RUN mkdir /tool-source

COPY ./poetry.lock /tool-source
COPY ./pyproject.toml /tool-source
COPY ./README.md /tool-source
COPY ./nomnomdata /tool-source/nomnomdata

RUN pip install /tool-source
