# Installation

`pip install nomnomdata-tools-engine nomnomdata-engine`

# Creating a new app

Instructions for how to create, build and deploy a new app available here:

<https://support.nomnomdata.com/portal/en/kb/articles/creating-your-first-nnd-app>

Additional toolkit information available here:

<http://developer.nomnomdata.com/>
